use adw::prelude::*;
use gtk::{
    gdk, gio, glib,
    glib::{clone, GString},
    subclass::prelude::*,
};
use gtk_macros::spawn;
use isahc::AsyncReadResponseExt;
use log::error;

mod imp {
    use std::cell::RefCell;

    use adw::subclass::prelude::BinImpl;
    use gtk::CompositeTemplate;
    use once_cell::sync::Lazy;

    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/sm/puri/AppMetadataEditor/ui/media.ui")]
    pub struct Media {
        pub url: RefCell<Option<GString>>,
        #[template_child]
        pub stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub media: TemplateChild<gtk::Image>,
        #[template_child]
        pub error: TemplateChild<gtk::Image>,
        #[template_child]
        pub spinner: TemplateChild<gtk::Spinner>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Media {
        const NAME: &'static str = "Media";
        type Type = super::Media;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Media {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpecString::new(
                    "url",
                    "Url",
                    "The url to the location",
                    None,
                    glib::ParamFlags::READWRITE | glib::ParamFlags::EXPLICIT_NOTIFY,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "url" => obj.set_url(value.get().unwrap()),
                _ => unimplemented!(),
            }
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "url" => obj.url().to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for Media {}
    impl BinImpl for Media {}
}

glib::wrapper! {
    pub struct Media(ObjectSubclass<imp::Media>)
        @extends gtk::Widget, adw::Bin,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl Media {
    pub fn new(url: &str) -> Self {
        glib::Object::new(&[("url", &url)]).expect("Failed to create Media")
    }

    pub fn url(&self) -> Option<GString> {
        self.imp().url.borrow().clone()
    }

    pub fn set_url(&self, url: Option<GString>) {
        let priv_ = self.imp();
        priv_.url.replace(url);
        priv_.stack.set_visible_child(&*priv_.spinner);
        priv_.spinner.set_spinning(true);

        spawn!(clone!(@weak self as obj => async move {
            match obj.download().await {
                Ok(()) => {
                    obj.imp().stack.set_visible_child(&*obj.imp().media);
                }
                Err(error) => {
                    obj.imp().stack.set_visible_child(&*obj.imp().error);
                    error!("Couldn't load image: {:?}", error);
                }
            }
            obj.imp().spinner.set_spinning(false);
        }));

        self.notify("url");
    }

    async fn download(&self) -> Result<(), isahc::Error> {
        if let Some(url) = self.url() {
            let mut response = isahc::get_async(url.as_str()).await?;

            let bytes: Vec<u8> = response.bytes().await.unwrap();

            let bytes = glib::Bytes::from_owned(bytes);
            let texture = gdk::Texture::from_bytes(&bytes).unwrap();
            self.imp().media.set_paintable(Some(&texture));
        }
        Ok(())
    }
}
