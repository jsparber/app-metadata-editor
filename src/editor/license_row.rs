use adw::prelude::*;
use gettextrs::gettext;
use gtk::{gio, glib, glib::clone, subclass::prelude::*};

use crate::editor::{custom_license::CustomLicense, license::License, LicenseList};

mod imp {
    use adw::subclass::{
        action_row::ActionRowImpl, combo_row::ComboRowImpl, preferences_row::PreferencesRowImpl,
    };
    use once_cell::sync::Lazy;

    use super::*;

    #[derive(Debug, Default)]
    pub struct LicenseRow {}

    #[glib::object_subclass]
    impl ObjectSubclass for LicenseRow {
        const NAME: &'static str = "LicenseRow";
        type Type = super::LicenseRow;
        type ParentType = adw::ComboRow;
    }

    impl ObjectImpl for LicenseRow {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpecString::new(
                    "selected-license",
                    "Selected License",
                    "The SPDX identifier of the selected license",
                    None,
                    glib::ParamFlags::READWRITE | glib::ParamFlags::EXPLICIT_NOTIFY,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "selected-license" => {
                    obj.set_selected_license(value.get::<Option<&str>>().unwrap())
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "selected-license" => obj.selected_license().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            let factory = gtk::SignalListItemFactory::new();

            factory.connect_setup(|_, list_item| {
                let container = gtk::Box::new(gtk::Orientation::Horizontal, 0);
                container.set_hexpand(true);
                let icon = gtk::Image::from_icon_name("object-select-symbolic");
                icon.set_halign(gtk::Align::End);
                icon.set_hexpand(true);
                container.append(&icon);
                list_item.set_child(Some(&container));
                // list_item.bind_property("selected", &icon,
                // "visible").flags(glib::BindingFlags::SYNC_CREATE).build();

                let label = gtk::Label::builder().xalign(0.0).build();
                container.prepend(&label);
            });

            factory.connect_bind(clone!(@weak obj => move |_, list_item| {
                let container = list_item.child().unwrap().downcast::<gtk::Box>().unwrap();
                let first_widget = container.first_child().unwrap();
                if let Some(item) = list_item
                    .item()
                    .and_then(|item| item.downcast::<License>().ok())
                {
                    if let Some(custom_license) = item.downcast_ref::<CustomLicense>() {
                        let entry = if let Some(entry) = first_widget.downcast_ref::<gtk::Entry>() {
                            entry.clone()
                        } else {
                            container.remove(&first_widget);
                            let entry = gtk::Entry::new();
                            entry.set_placeholder_text(Some(&gettext("License URL")));
                            container.prepend(&entry);
                            entry
                        };
                        let binding = custom_license
                        .bind_property("custom", &entry, "text")
                        .flags(glib::BindingFlags::SYNC_CREATE | glib::BindingFlags::BIDIRECTIONAL)
                        .build();

                        unsafe {
                            list_item.set_data("BINDING", binding);
                        }
                        let signal_id = entry.connect_activate(clone!(@weak obj, @weak list_item => move |entry| {
                                // NOTE: this doesn't work if the row is inside a Popover
                                if let Some(popover) = entry.ancestor(gtk::Popover::static_type()) {
                                    popover.downcast::<gtk::Popover>().unwrap().popdown();
                                }
                                obj.set_selected(list_item.position());
                            }));

                        unsafe {
                            list_item.set_data("ENTRY_SIGNAL_ID", signal_id);
                        }
                    } else {
                        if let Some(label) = first_widget.downcast_ref::<gtk::Label>() {
                            label.set_label(item.identifier());
                        } else {
                            container.remove(&first_widget);
                            let label = gtk::Label::builder()
                                .xalign(0.0)
                                .label(item.identifier())
                                .build();
                            container.prepend(&label);
                        }
                    }
                }

                if container.ancestor(gtk::Popover::static_type()).is_none() {
                    container.last_child().unwrap().hide();
                }

                let handler_id = obj.connect_selected_item_notify(clone!(@weak list_item => move |obj| {
                    if let Some(container) = list_item.child() {
                        let icon = container.last_child().unwrap();

                        if obj.selected_item() == list_item.item() {
                            icon.set_opacity(1.0);
                        } else {
                            icon.set_opacity(0.0);
                        }
                    }
                }));

                unsafe {
                    list_item.set_data("SIGNAL_ID", handler_id);
                }

                let icon = container.last_child().unwrap();
                if obj.selected_item() == list_item.item() {
                    icon.set_opacity(1.0);
                } else {
                    icon.set_opacity(0.0);
                }
            }));

            factory.connect_unbind(clone!(@weak obj => move|_, list_item| {
                let handler = unsafe {
                    list_item.steal_data("SIGNAL_ID").unwrap()
                };
                obj.disconnect(handler);

                if let Some(child) = list_item.child() {
                    let handler = unsafe {
                        list_item.steal_data("ENTRY_SIGNAL_ID")
                    };

                    if let Some(handler) = handler {
                        child.first_child().unwrap().disconnect(handler);
                    }
                }

                let binding: Option<glib::Binding> =
                unsafe {
                    list_item.steal_data("BINDING")
                };

                if let Some(binding) = binding {
                    binding.unbind();
                }
            }));

            obj.set_factory(Some(&factory));

            obj.connect_selected_notify(|obj| {
                obj.notify("selected-license");
            });
        }
    }

    impl WidgetImpl for LicenseRow {}
    impl ActionRowImpl for LicenseRow {}
    impl ComboRowImpl for LicenseRow {}
    impl PreferencesRowImpl for LicenseRow {}
    impl ListBoxRowImpl for LicenseRow {}
}

glib::wrapper! {
    pub struct LicenseRow(ObjectSubclass<imp::LicenseRow>)
        @extends gtk::Widget, adw::ComboRow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl LicenseRow {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create LicenseRow")
    }

    pub fn license_list(&self) -> Option<LicenseList> {
        self.model()?.downcast::<LicenseList>().ok()
    }

    pub fn selected_license(&self) -> Option<String> {
        let item = self.selected_item()?.downcast::<License>().ok()?;
        Some(item.identifier().to_owned())
    }

    pub fn set_selected_license(&self, identifier: Option<impl AsRef<str>>) {
        if let Some(identifier) = identifier {
            let identifier: &str = identifier.as_ref();
            if let Some(position) = self
                .license_list()
                .and_then(|model| model.find_position_by_identifier(identifier))
            {
                self.set_selected(position);
            }
        }
    }
}
