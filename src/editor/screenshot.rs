use adw::prelude::*;
use appstream::prelude::{ImageExt, ScreenshotExt};
use gtk::{gio, glib, subclass::prelude::*};

use super::media;

mod imp {
    use adw::subclass::prelude::BinImpl;
    use gtk::CompositeTemplate;
    use once_cell::{sync::Lazy, unsync::OnceCell};

    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/sm/puri/AppMetadataEditor/ui/screenshot.ui")]
    pub struct Screenshot {
        pub screenshot: OnceCell<appstream::Screenshot>,
        #[template_child]
        pub media: TemplateChild<media::Media>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Screenshot {
        const NAME: &'static str = "Screenshot";
        type Type = super::Screenshot;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            media::Media::static_type();
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Screenshot {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpecObject::new(
                    "screenshot",
                    "Screenshot",
                    "The screenshot appstream object",
                    appstream::Screenshot::static_type(),
                    glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "screenshot" => obj.set_screenshot(value.get().unwrap()),
                _ => unimplemented!(),
            }
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "screenshot" => obj.screenshot().to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for Screenshot {}
    impl BinImpl for Screenshot {}
}

glib::wrapper! {
    pub struct Screenshot(ObjectSubclass<imp::Screenshot>)
        @extends gtk::Widget, adw::ActionRow, adw::PreferencesRow, gtk::ListBoxRow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl Screenshot {
    pub fn new(screenshot: &appstream::Screenshot) -> Self {
        glib::Object::new(&[("screenshot", screenshot)]).expect("Failed to create Screenshot")
    }

    pub fn screenshot(&self) -> &appstream::Screenshot {
        self.imp()
            .screenshot
            .get()
            .expect("The screenshot property should be set")
    }

    fn set_screenshot(&self, screenshot: appstream::Screenshot) {
        if let Some(image) = screenshot.image(92, 92) {
            self.imp().media.set_url(image.url());
        }

        self.imp()
            .screenshot
            .set(screenshot)
            .expect("The screenshot property can be set only once");
    }
}
