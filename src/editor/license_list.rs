use gtk::{gio, glib, prelude::*, subclass::prelude::*};

use super::{custom_license::CustomLicense, license::License};

static METADATA_LICENSE: [&'static str; 14] = [
    "FSFAP",
    "MIT",
    "0BSD",
    "CC0-1.0",
    "CC-BY-3.0",
    "CC-BY-4.0",
    "CC-BY-SA-3.0",
    "CC-BY-SA-4.0",
    "GFDL-1.1",
    "GFDL-1.2",
    "GFDL-1.3",
    "BSL-1.0",
    "FTL",
    "FSFUL",
];

static PROJECT_LICENSE: [&'static str; 5] = [
    "GPL-2.0",
    "LGPL-3.0+",
    "GPL-3.0+",
    "MIT",
    "CC-BY-SA-2.0",
    // "LicenseRef-proprietary=https://example.com/mylicense.html",
];

#[derive(Debug, Copy, Clone, PartialEq, Eq, glib::Enum)]
#[enum_type(name = "LicenseType")]
pub enum LicenseType {
    #[enum_value(name = "LicenseTypeMetadata")]
    Metadata,
    #[enum_value(name = "LicenseTypeProject")]
    Project,
}

impl Default for LicenseType {
    fn default() -> Self {
        LicenseType::Metadata
    }
}

mod imp {
    use std::cell::Cell;

    use glib::object::WeakRef;
    use once_cell::{sync::Lazy, unsync::OnceCell};

    use super::*;

    #[derive(Debug, Default)]
    pub struct LicenseList {
        pub license_type: Cell<LicenseType>,
        pub list: OnceCell<Vec<License>>,
        pub custom_license: WeakRef<CustomLicense>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LicenseList {
        const NAME: &'static str = "LicenseList";
        type Type = super::LicenseList;
        type Interfaces = (gio::ListModel,);
    }

    impl ObjectImpl for LicenseList {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![
                    glib::ParamSpecEnum::new(
                        "license-type",
                        "License Type",
                        "The type of this License list",
                        LicenseType::static_type(),
                        LicenseType::default() as i32,
                        glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
                    ),
                    glib::ParamSpecObject::new(
                        "custom-license",
                        "Custom License",
                        "The the custom license if any",
                        CustomLicense::static_type(),
                        glib::ParamFlags::READABLE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            _obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "license-type" => self.license_type.set(value.get::<LicenseType>().unwrap()),
                _ => unimplemented!(),
            }
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "license-type" => obj.license_type().to_value(),
                "custom-license" => obj.custom_license().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            let list = match obj.license_type() {
                LicenseType::Metadata => METADATA_LICENSE
                    .iter()
                    .map(|license| License::new(license))
                    .collect(),
                LicenseType::Project => {
                    let mut list: Vec<License> = PROJECT_LICENSE
                        .iter()
                        .map(|license| License::new(license))
                        .collect();
                    let custom = CustomLicense::new("proprietary");
                    self.custom_license.set(Some(&custom));
                    list.push(custom.upcast::<License>());

                    list
                }
            };

            self.list.set(list).unwrap();
        }
    }

    impl ListModelImpl for LicenseList {
        fn item_type(&self, _list_model: &Self::Type) -> glib::Type {
            License::static_type()
        }

        fn n_items(&self, _list_model: &Self::Type) -> u32 {
            self.list.get().unwrap().len() as u32
        }

        fn item(&self, _list_model: &Self::Type, position: u32) -> Option<glib::Object> {
            self.list
                .get()
                .unwrap()
                .get(position as usize)
                .cloned()
                .map(|item| item.upcast())
        }
    }
}

glib::wrapper! {
    pub struct LicenseList(ObjectSubclass<imp::LicenseList>)
        @implements gio::ListModel;
}

impl LicenseList {
    pub fn new(license_type: &LicenseType) -> Self {
        glib::Object::new(&[("license-type", license_type)]).expect("Failed to create LicenseList")
    }

    pub fn license_type(&self) -> LicenseType {
        self.imp().license_type.get()
    }

    pub fn custom_license(&self) -> Option<CustomLicense> {
        self.imp().custom_license.upgrade()
    }

    pub fn find_position_by_identifier(&self, identifier: &str) -> Option<u32> {
        Some(
            self.imp()
                .list
                .get()?
                .iter()
                .position(|obj| obj.identifier() == identifier)? as u32,
        )
    }
}
