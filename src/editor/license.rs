use gtk::{glib, prelude::*, subclass::prelude::*};

mod imp {
    use once_cell::{sync::Lazy, unsync::OnceCell};

    use super::*;

    #[derive(Debug, Default)]
    pub struct License {
        pub identifier: OnceCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for License {
        const NAME: &'static str = "License";
        type Type = super::License;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for License {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpecString::new(
                    "identifier",
                    "Identifier",
                    "The SPDX identifier of this license",
                    None,
                    glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            _obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "identifier" => self.identifier.set(value.get().unwrap()).unwrap(),
                _ => unimplemented!(),
            }
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "identifier" => obj.identifier().to_value(),
                _ => unimplemented!(),
            }
        }
    }
}

unsafe impl<T> IsSubclassable<T> for License
where
    T: LicenseImpl,
    T::Type: IsA<License>,
{
}

pub trait LicenseImpl: ObjectImpl {}

glib::wrapper! {
    pub struct License(ObjectSubclass<imp::License>);
}

impl License {
    pub fn new(identifier: &str) -> Self {
        glib::Object::new(&[("identifier", &identifier)]).expect("Failed to create License")
    }

    pub fn identifier(&self) -> &str {
        self.imp().identifier.get().unwrap()
    }
}
