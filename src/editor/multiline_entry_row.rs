use adw::prelude::*;
use gtk::{gio, glib, subclass::prelude::*};

mod imp {
    use adw::subclass::prelude::{ActionRowImpl, EntryRowImpl, PreferencesRowImpl};

    use super::*;

    #[derive(Debug, Default)]
    pub struct MultilineEntryRow {}

    #[glib::object_subclass]
    impl ObjectSubclass for MultilineEntryRow {
        const NAME: &'static str = "MultilineEntryRow";
        type Type = super::MultilineEntryRow;
        type ParentType = adw::EntryRow;
    }

    impl ObjectImpl for MultilineEntryRow {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);
            let container = obj
                .first_child()
                .unwrap()
                .first_child()
                .unwrap()
                .next_sibling()
                .unwrap();
            let text = container
                .last_child()
                .unwrap()
                .downcast::<gtk::Text>()
                .unwrap();
            text.unparent();

            let view = gtk::Text::new();
            view.insert_before(&container, gtk::Widget::NONE);
        }
    }

    impl WidgetImpl for MultilineEntryRow {}
    impl ListBoxRowImpl for MultilineEntryRow {}
    impl PreferencesRowImpl for MultilineEntryRow {}
    impl ActionRowImpl for MultilineEntryRow {}
    impl EntryRowImpl for MultilineEntryRow {}
}

glib::wrapper! {
    pub struct MultilineEntryRow(ObjectSubclass<imp::MultilineEntryRow>)
        @extends gtk::Widget, adw::ActionRow, adw::PreferencesRow, gtk::ListBoxRow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl MultilineEntryRow {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create MultilineEntryRow")
    }
}
