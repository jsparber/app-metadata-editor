use gtk::{glib, prelude::*, subclass::prelude::*};

use crate::editor::license::{License, LicenseImpl};

mod imp {
    use std::cell::RefCell;

    use once_cell::sync::Lazy;

    use super::*;

    #[derive(Debug, Default)]
    pub struct CustomLicense {
        pub custom: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CustomLicense {
        const NAME: &'static str = "CustomLicense";
        type Type = super::CustomLicense;
        type ParentType = License;
    }

    impl ObjectImpl for CustomLicense {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpecString::new(
                    "custom",
                    "custom",
                    "The custom license",
                    None,
                    glib::ParamFlags::READWRITE | glib::ParamFlags::EXPLICIT_NOTIFY,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "custom" => obj.set_custom(value.get().unwrap()),
                _ => unimplemented!(),
            }
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "custom" => obj.custom().to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl LicenseImpl for CustomLicense {}
}

glib::wrapper! {
    pub struct CustomLicense(ObjectSubclass<imp::CustomLicense>)
    @extends License;
}

impl CustomLicense {
    pub fn new(identifier: &str) -> Self {
        glib::Object::new(&[("identifier", &identifier)]).expect("Failed to create CustomLicense")
    }

    pub fn custom(&self) -> String {
        self.imp().custom.borrow().clone()
    }

    pub fn set_custom(&self, custom: String) {
        self.imp().custom.replace(custom);
        self.notify("custom");
    }
}
