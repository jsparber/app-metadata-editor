use adw::prelude::*;
use appstream::prelude::ReleaseExt;
use gettextrs::gettext;
use gtk::{gio, glib, subclass::prelude::*};
use log::warn;

mod imp {
    use adw::subclass::prelude::{ActionRowImpl, PreferencesRowImpl};
    use gtk::CompositeTemplate;
    use once_cell::{sync::Lazy, unsync::OnceCell};

    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/sm/puri/AppMetadataEditor/ui/release.ui")]
    pub struct Release {
        pub release: OnceCell<appstream::Release>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Release {
        const NAME: &'static str = "Release";
        type Type = super::Release;
        type ParentType = adw::ActionRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Release {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpecObject::new(
                    "release",
                    "Release",
                    "The release appstream object",
                    appstream::Release::static_type(),
                    glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "release" => obj.set_release(value.get().unwrap()),
                _ => unimplemented!(),
            }
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "release" => obj.release().to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for Release {}
    impl ListBoxRowImpl for Release {}
    impl PreferencesRowImpl for Release {}
    impl ActionRowImpl for Release {}
}

glib::wrapper! {
    pub struct Release(ObjectSubclass<imp::Release>)
        @extends gtk::Widget, adw::ActionRow, adw::PreferencesRow, gtk::ListBoxRow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl Release {
    pub fn new(release: &appstream::Release) -> Self {
        glib::Object::new(&[("release", release)]).expect("Failed to create Release")
    }

    pub fn release(&self) -> &appstream::Release {
        self.imp()
            .release
            .get()
            .expect("The release property should be set")
    }

    fn set_release(&self, release: appstream::Release) {
        if let Some(version) = release.version() {
            self.set_title(version.as_str());
        }
        if let Some(date) = format_date(release.timestamp()) {
            self.set_subtitle(date.as_str());
        } else {
            warn!("No valid timestamp was set for this release");
        }

        self.imp()
            .release
            .set(release)
            .expect("The release property can be set only once");
    }
}

fn format_date(timestamp: u64) -> Option<glib::GString> {
    let date = glib::DateTime::from_unix_utc(timestamp as i64).ok()?;
    // Translators: This is the format for the date of a release
    date.format(&gettext("%b %e, %Y")).ok()
}
