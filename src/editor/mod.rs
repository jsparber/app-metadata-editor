use adw::prelude::PreferencesGroupExt;
use appstream::{prelude::*, UrlKind};
use gettextrs::gettext;
use gtk::{gio, gio::prelude::FileExt, glib, glib::clone, prelude::*, subclass::prelude::*};
use log::debug;

use crate::CustomEntry;
mod custom_license;
mod license;
mod license_list;
mod license_row;
mod media;
mod multiline_entry_row;
mod release;
mod screenshot;

use license_list::{LicenseList, LicenseType};
use license_row::LicenseRow;

mod imp {
    use std::cell::RefCell;

    use adw::subclass::prelude::BinImpl;
    use gtk::CompositeTemplate;
    use once_cell::sync::Lazy;

    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/sm/puri/AppMetadataEditor/ui/editor.ui")]
    pub struct Editor {
        pub file: RefCell<Option<gio::File>>,
        #[template_child]
        pub app_name: TemplateChild<adw::EntryRow>,
        #[template_child]
        pub developer_name: TemplateChild<adw::EntryRow>,
        #[template_child]
        pub app_summary: TemplateChild<adw::EntryRow>,
        #[template_child]
        pub web_links: TemplateChild<adw::PreferencesGroup>,
        #[template_child]
        pub releases: TemplateChild<adw::PreferencesGroup>,
        #[template_child]
        pub screenshots: TemplateChild<gtk::FlowBox>,
        #[template_child]
        pub project_license: TemplateChild<LicenseRow>,
        #[template_child]
        pub metadata_license: TemplateChild<LicenseRow>,
        pub metadata: RefCell<Option<appstream::Metadata>>,
        pub monitor: RefCell<Option<gio::FileMonitor>>,
        pub releases_widgets: RefCell<Vec<glib::WeakRef<gtk::Widget>>>,
        pub web_links_widgets: RefCell<Vec<glib::WeakRef<gtk::Widget>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Editor {
        const NAME: &'static str = "Editor";
        type Type = super::Editor;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            adw::SplitButton::static_type();
            sourceview::View::static_type();
            CustomEntry::static_type();
            LicenseList::static_type();
            LicenseType::static_type();
            multiline_entry_row::MultilineEntryRow::static_type();
            LicenseRow::static_type();
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Editor {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![glib::ParamSpecObject::new(
                    "file",
                    "File",
                    "The file used in the Editor",
                    gio::File::static_type(),
                    glib::ParamFlags::READWRITE | glib::ParamFlags::EXPLICIT_NOTIFY,
                )]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "file" => obj.set_file(value.get().unwrap()),
                _ => unimplemented!(),
            }
        }

        fn property(&self, obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "file" => obj.file().to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for Editor {}
    impl BinImpl for Editor {}
}

glib::wrapper! {
    pub struct Editor(ObjectSubclass<imp::Editor>)
        @extends gtk::Widget,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl Editor {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create Editor")
    }

    pub fn file(&self) -> Option<gio::File> {
        self.imp().file.borrow().clone()
    }

    pub fn set_file(&self, file: Option<gio::File>) {
        let priv_ = self.imp();

        if priv_.file.borrow().as_ref() == file.as_ref() {
            return;
        }

        self.clean();

        let file = if let Some(file) = file {
            file
        } else {
            priv_.file.take();
            self.notify("file");
            return;
        };

        let monitor = file
            .monitor_file(gio::FileMonitorFlags::WATCH_MOUNTS, gio::Cancellable::NONE)
            .unwrap();
        monitor.set_rate_limit(500);

        monitor.connect_changed(clone!(@weak self as obj => move |_, _, _, _| {
            debug!("Update file because it changed.");
            let metadata = obj.imp().metadata.borrow();
            let file = obj.imp().file.borrow();
            metadata.as_ref().unwrap().parse_file(file.as_ref().unwrap(), appstream::FormatKind::Xml).unwrap();
        }));

        let metadata = appstream::Metadata::new();
        metadata
            .parse_file(&file, appstream::FormatKind::Xml)
            .unwrap();
        metadata.set_update_existing(true);
        let component = metadata.component().unwrap();
        priv_.metadata.replace(Some(metadata));

        priv_.monitor.replace(Some(monitor));
        component
            .bind_property("developer-name", &*priv_.developer_name, "text")
            .flags(glib::BindingFlags::SYNC_CREATE | glib::BindingFlags::BIDIRECTIONAL)
            .build();
        component
            .bind_property("name", &*priv_.app_name, "text")
            .flags(glib::BindingFlags::SYNC_CREATE | glib::BindingFlags::BIDIRECTIONAL)
            .build();
        component
            .bind_property("summary", &*priv_.app_summary, "text")
            .flags(glib::BindingFlags::SYNC_CREATE | glib::BindingFlags::BIDIRECTIONAL)
            .build();

        for screenshot in component.screenshots() {
            let widget = screenshot::Screenshot::new(&screenshot);
            priv_.screenshots.append(&widget);
        }

        let mut releases_widgets = priv_.releases_widgets.borrow_mut();
        for release in component.releases() {
            let widget = release::Release::new(&release);
            priv_.releases.add(&widget);
            releases_widgets.push(widget.upcast::<gtk::Widget>().downgrade());
        }

        let url_kinds: [(String, UrlKind); 8] = [
            (gettext("Homepage"), UrlKind::Homepage),
            (gettext("Bug Tracker"), UrlKind::Bugtracker),
            (gettext("Faq"), UrlKind::Faq),
            (gettext("Help"), UrlKind::Help),
            (gettext("Donations"), UrlKind::Donation),
            (gettext("Translations"), UrlKind::Translate),
            (gettext("Contact"), UrlKind::Contact),
            (gettext("Unknown"), UrlKind::Unknown),
        ];

        let mut web_links = priv_.web_links_widgets.borrow_mut();
        for (title, kind) in url_kinds {
            let url = component.url(kind);

            // Only show Unknown url if it was set already
            if kind == UrlKind::Unknown && url.is_none() {
                continue;
            }
            let builder = adw::EntryRow::builder().title(&title);
            let builder = if let Some(url) = url {
                builder.text(&url)
            } else {
                builder
            };

            let widget = builder.build();

            priv_.web_links.add(&widget);
            web_links.push(widget.upcast::<gtk::Widget>().downgrade());
        }

        priv_
            .metadata_license
            .get()
            .set_selected_license(component.metadata_license());
        priv_
            .project_license
            .get()
            .set_selected_license(component.project_license());

        priv_.file.replace(Some(file));
        self.notify("file");
        debug!("A new file was set and loaded.");
    }

    fn clean(&self) {
        let priv_ = self.imp();

        // Remove screenshots
        let guard = priv_.screenshots.freeze_notify();
        while let Some(child) = priv_.screenshots.child_at_index(0) {
            priv_.screenshots.remove(&child);
        }

        drop(guard);

        // Remove releases
        let guard = priv_.releases.freeze_notify();
        let mut releases_widgets = priv_.releases_widgets.borrow_mut();
        while let Some(child) = releases_widgets.pop() {
            if let Some(child) = child.upgrade() {
                priv_.releases.remove(&child);
            }
        }

        drop(guard);

        // Remove web links
        let guard = priv_.web_links.freeze_notify();
        let mut web_links_widgets = priv_.web_links_widgets.borrow_mut();
        while let Some(child) = web_links_widgets.pop() {
            if let Some(child) = child.upgrade() {
                priv_.web_links.remove(&child);
            }
        }

        drop(guard);
    }
}
