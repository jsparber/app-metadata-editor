use adw;
use gettextrs::gettext;
use glib::clone;
use gtk::{gdk, gio, glib, prelude::*, subclass::prelude::*};
use log::{debug, info};

use crate::{
    config::{APP_ID, PKGDATADIR, PROFILE, VERSION},
    window::AppMetadataEditorApplicationWindow,
};

mod imp {
    use adw::subclass::prelude::AdwApplicationImpl;
    use glib::WeakRef;
    use once_cell::sync::OnceCell;

    use super::*;

    #[derive(Debug, Default)]
    pub struct AppMetadataEditorApplication {
        pub window: OnceCell<WeakRef<AppMetadataEditorApplicationWindow>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AppMetadataEditorApplication {
        const NAME: &'static str = "AppMetadataEditorApplication";
        type Type = super::AppMetadataEditorApplication;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for AppMetadataEditorApplication {}

    impl ApplicationImpl for AppMetadataEditorApplication {
        fn activate(&self, app: &Self::Type) {
            debug!("GtkApplication<AppMetadataEditorApplication>::activate");
            self.parent_activate(app);

            if let Some(window) = self.window.get() {
                let window = window.upgrade().unwrap();
                window.present();
                return;
            }

            let window = AppMetadataEditorApplicationWindow::new(app);
            self.window
                .set(window.downgrade())
                .expect("Window already set.");

            app.main_window().present();
        }

        fn startup(&self, app: &Self::Type) {
            debug!("GtkApplication<AppMetadataEditorApplication>::startup");
            self.parent_startup(app);

            // Set icons for shell
            gtk::Window::set_default_icon_name(APP_ID);

            app.setup_css();
            app.setup_gactions();
            app.setup_accels();
        }
    }

    impl GtkApplicationImpl for AppMetadataEditorApplication {}
    impl AdwApplicationImpl for AppMetadataEditorApplication {}
}

glib::wrapper! {
    pub struct AppMetadataEditorApplication(ObjectSubclass<imp::AppMetadataEditorApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl AppMetadataEditorApplication {
    pub fn new() -> Self {
        glib::Object::new(&[
            ("application-id", &Some(APP_ID)),
            ("flags", &gio::ApplicationFlags::empty()),
            ("resource-base-path", &Some("/sm/puri/AppMetadataEditor/")),
        ])
        .expect("Application initialization failed...")
    }

    fn main_window(&self) -> AppMetadataEditorApplicationWindow {
        self.imp().window.get().unwrap().upgrade().unwrap()
    }

    fn setup_gactions(&self) {
        // Quit
        let action_quit = gio::SimpleAction::new("quit", None);
        action_quit.connect_activate(clone!(@weak self as app => move |_, _| {
            // This is needed to trigger the delete event and saving the window state
            app.main_window().close();
            app.quit();
        }));
        self.add_action(&action_quit);

        // About
        let action_about = gio::SimpleAction::new("about", None);
        action_about.connect_activate(clone!(@weak self as app => move |_, _| {
            app.show_about_dialog();
        }));
        self.add_action(&action_about);
    }

    // Sets up keyboard shortcuts
    fn setup_accels(&self) {
        self.set_accels_for_action("app.quit", &["<Control>q"]);
    }

    fn setup_css(&self) {
        let provider = gtk::CssProvider::new();
        provider.load_from_resource("/sm/puri/AppMetadataEditor/style.css");
        if let Some(display) = gdk::Display::default() {
            gtk::StyleContext::add_provider_for_display(
                &display,
                &provider,
                gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
            );
        }
    }

    fn show_about_dialog(&self) {
        let dialog = adw::AboutWindow::builder()
            .application_icon(APP_ID)
            .license_type(gtk::License::Gpl30)
            .website("https://gitlab.gnome.org/Community/Purism/app-metadata-editor")
            .version(VERSION)
            .transient_for(&self.main_window())
            .translator_credits(&gettext("translator-credits"))
            .modal(true)
            .developers(vec!["Julian Sparber".into()])
            .designers(vec!["Tobias Bernard".into()])
            .build();

        dialog.present();
    }

    pub fn run(&self) {
        info!("App Metadata Editor ({})", APP_ID);
        info!("Version: {} ({})", VERSION, PROFILE);
        info!("Datadir: {}", PKGDATADIR);

        ApplicationExtManual::run(self);
    }
}
