use gettextrs::gettext;
use gtk::{gdk, gio, glib, glib::clone, prelude::*, subclass::prelude::*};
use gtk_macros::spawn;

use crate::{
    application::AppMetadataEditorApplication,
    config::{APP_ID, PROFILE},
    editor::Editor,
};

mod imp {
    use adw::subclass::prelude::AdwApplicationWindowImpl;
    use gtk::CompositeTemplate;

    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/sm/puri/AppMetadataEditor/ui/window.ui")]
    pub struct AppMetadataEditorApplicationWindow {
        #[template_child]
        pub headerbar: TemplateChild<gtk::HeaderBar>,
        #[template_child]
        pub editor: TemplateChild<Editor>,
        pub settings: gio::Settings,
    }

    impl Default for AppMetadataEditorApplicationWindow {
        fn default() -> Self {
            Self {
                headerbar: TemplateChild::default(),
                editor: TemplateChild::default(),
                settings: gio::Settings::new(APP_ID),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AppMetadataEditorApplicationWindow {
        const NAME: &'static str = "AppMetadataEditorApplicationWindow";
        type Type = super::AppMetadataEditorApplicationWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            adw::SplitButton::static_type();
            Editor::static_type();
            Self::bind_template(klass);

            klass.add_binding_action(
                gdk::Key::o,
                gdk::ModifierType::CONTROL_MASK,
                "win.open-file-chooser",
                None,
            );

            klass.install_action("win.open-file-chooser", None, move |widget, _, _| {
                widget.open_file_chooser();
            });

            klass.install_action("win.open-file", None, move |_widget, _, _| {
                // widget.open_file();
            });
        }

        // You must call `Widget`'s `init_template()` within `instance_init()`.
        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AppMetadataEditorApplicationWindow {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            // Devel Profile
            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            // Load latest window state
            obj.load_window_size();
        }
    }

    impl WidgetImpl for AppMetadataEditorApplicationWindow {}
    impl WindowImpl for AppMetadataEditorApplicationWindow {
        // Save window state on delete event
        fn close_request(&self, window: &Self::Type) -> gtk::Inhibit {
            if let Err(err) = window.save_window_size() {
                log::warn!("Failed to save window state, {}", &err);
            }

            // Pass close request on to the parent
            self.parent_close_request(window)
        }
    }

    impl ApplicationWindowImpl for AppMetadataEditorApplicationWindow {}
    impl AdwApplicationWindowImpl for AppMetadataEditorApplicationWindow {}
}

glib::wrapper! {
    pub struct AppMetadataEditorApplicationWindow(ObjectSubclass<imp::AppMetadataEditorApplicationWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl AppMetadataEditorApplicationWindow {
    pub fn new(app: &AppMetadataEditorApplication) -> Self {
        glib::Object::new(&[("application", app)])
            .expect("Failed to create AppMetadataEditorApplicationWindow")
    }

    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let imp = self.imp();

        let (width, height) = self.default_size();

        imp.settings.set_int("window-width", width)?;
        imp.settings.set_int("window-height", height)?;

        imp.settings
            .set_boolean("is-maximized", self.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let imp = self.imp();

        let width = imp.settings.int("window-width");
        let height = imp.settings.int("window-height");
        let is_maximized = imp.settings.boolean("is-maximized");

        self.set_default_size(width, height);

        if is_maximized {
            self.maximize();
        }
    }

    fn open_file_chooser(&self) {
        let filter = gtk::FileFilter::new();
        filter.add_mime_type("text/xml");
        filter.add_mime_type("application/xml");
        filter.set_name(Some(&gettext("Metadata")));

        let dialog = gtk::FileChooserNative::builder()
            .title(&gettext("Choose Metadata File"))
            .modal(true)
            .transient_for(self)
            .action(gtk::FileChooserAction::Open)
            .build();

        // FIXME: for some reason setting a filter via the builder doesn't work
        dialog.add_filter(&filter);

        spawn!(clone!(@weak self as obj => async move {
            if dialog.run_future().await == gtk::ResponseType::Accept {
                if let Some(file) = dialog.file() {
                    obj.imp().editor.set_file(Some(file));
                }
            }
        }));
    }
}
