option(
  'profile',
  type: 'combo',
  choices: [
    'default',
    'development'
  ],
  value: 'default',
  description: 'The build profile for App Metadata Editor. One of "default" or "development".'
)
